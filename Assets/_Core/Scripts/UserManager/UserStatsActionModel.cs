﻿using System;

namespace _Core
{
     public class UserStatsActionModel
     {
          public Action<Statistics> onCoinCollected;
          public Action<Statistics> onDistanceRun;
          public Action<UniqueEventsData> onUniqueEvent;
     }
}