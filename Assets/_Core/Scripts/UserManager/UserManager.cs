﻿using System;
using TMPro.EditorUtilities;

namespace _Core
{
    public class UserManager : Singleton<UserManager>
    {
        private const string UserDataKey = "UserData";
        private const string StatisticsDataKey = "StatisticsData";
        private const string UniqueEventsDataKey = "UniqueEventsData";
        
        private event Action<Statistics> OnCoinCollected;
        private event Action<Statistics> OnDistanceRun; 
        private event Action<UniqueEventsData> OnUniqueEvent;

        private UserData _userData;
        private Statistics _statisticsData;
        private UniqueEventsData _uniqueEventsData;
        
        public static UserData UserData => _instance._userData;
        public static Statistics StatisticsData => _instance._statisticsData;
        public static UniqueEventsData UniqueEventsData => _instance._uniqueEventsData;

        protected override void Initialize()
        {
            LoadAllUserData();
        }
        
        public static bool TrySpendCoins(int coinsCount)
        {
            if (UserData.coins < coinsCount) return false;
            UserData.coins -= coinsCount;
            _instance.SaveUserData();
            return true;
        }

        public static void AddCoin(int amount)
        {
            _instance._userData.coins += amount;
            _instance.SaveAllUserData();
        }

        public static void AddCoinCollected(int amount)
        {
            _instance._statisticsData.global.coinsCollected += amount;
            _instance._statisticsData.lastRun.coinsCollected += amount;
            AddCoin(amount);
            _instance.OnCoinCollected?.Invoke(_instance._statisticsData);
        }

        public static void AddDistanceRun(float distance)
        {
            _instance._statisticsData.global.distancePassed += distance;
            _instance._statisticsData.lastRun.distancePassed += distance;
            _instance.OnDistanceRun?.Invoke(_instance._statisticsData);
        }

        public void InvokeUqinueEvent()
        {
            _instance.OnUniqueEvent?.Invoke(_instance._uniqueEventsData);
        }

        private void LoadAllUserData()
        {
            _userData = SaveManager.Load<UserData>(UserDataKey) ?? new UserData();
            _statisticsData = SaveManager.Load<Statistics>(StatisticsDataKey) ?? new Statistics();
            _uniqueEventsData = SaveManager.Load<UniqueEventsData>(UniqueEventsDataKey) ?? new UniqueEventsData();

            _userData = new UserData();
            _statisticsData = new Statistics();
            _uniqueEventsData = new UniqueEventsData();
        }
        
        public static void SubscribeToStatsEvents(UserStatsActionModel userStatsActionModel)
        {
            _instance.OnCoinCollected += userStatsActionModel.onCoinCollected;
            _instance.OnDistanceRun += userStatsActionModel.onDistanceRun;
            _instance.OnUniqueEvent += userStatsActionModel.onUniqueEvent;
        }
        
        private void SaveAllUserData()
        {
            SaveUserData();
            SaveStatisticsData();
            SaveUniqueEventsData();
        }

        private void SaveUserData()
        {
            SaveManager.Save(UserDataKey, _userData);
        }

        private void SaveStatisticsData()
        {
            SaveManager.Save(StatisticsDataKey, _statisticsData);
        }

        private void SaveUniqueEventsData()
        {
            SaveManager.Save(UniqueEventsDataKey, _uniqueEventsData);
        }

        public static void InvokeAllDataEvents()
        {
            _instance.OnCoinCollected?.Invoke(_instance._statisticsData);
            _instance.OnDistanceRun?.Invoke(_instance._statisticsData);
            _instance.OnUniqueEvent?.Invoke(_instance._uniqueEventsData);
        }

        public static void ResetLastGameProgress()
        {
            StatisticsData.lastRun = new StatisticsData();
            _instance.SaveStatisticsData();
        }
    }
}