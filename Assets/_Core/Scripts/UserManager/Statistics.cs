﻿using System;

namespace _Core
{
    [Serializable]
    public class Statistics
    {
        public StatisticsData global = new StatisticsData();
        public StatisticsData lastRun = new StatisticsData();
    }
}