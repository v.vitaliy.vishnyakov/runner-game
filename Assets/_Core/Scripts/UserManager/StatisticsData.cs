﻿using System;

namespace _Core
{
    [Serializable]
    public class StatisticsData
    {
        public int coinsCollected;
        public float distancePassed;
        public int bonusesCollected;
        public int magnetCollected;
        public int scoreBoostCollected;
        public int shieldCollected;
    }
}