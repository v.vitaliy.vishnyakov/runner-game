﻿using System;
using UnityEngine;

namespace _Core
{
    [RequireComponent(typeof(Collider), typeof(MeshRenderer))]
    public class Coin: MonoBehaviour, IPoolableObject
    {
        [SerializeField] private float _initialMagnetizedSpeed;
        [SerializeField] private float _acceleration;
        [SerializeField] private float _rotationSpeed;
        [SerializeField] private string _soundOnPickupKey;
        private float _currentSpeed;
        public bool IsMagnetized { get; set; }

        private void Awake()
        {
            Initialize();
        }

        private void Start()
        {
            GameManager.SubscribeToGameEvents(OnMoveBack, OnGameEnd);
        }

        private void Initialize()
        {
            _currentSpeed = _initialMagnetizedSpeed;
        }
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag(Tags.Player))
            {
                SoundManager.PlaySFX(_soundOnPickupKey);
                UserManager.AddCoinCollected(1);
                Disable();
            }
        }
        
        private void Update()
        {
            if (IsMagnetized)
            {
                MoveToPlayer();
            }
            Rotate();
        }

        private void MoveToPlayer()
        {
            Vector3 characterPosition = new Vector3(GameManager.CharacterPosition.x, GameManager.CharacterPosition.y + 1,
                GameManager.CharacterPosition.z);
            Vector3 direction = Utils.GetDirectionOnPoint(transform.position, characterPosition);
            _currentSpeed += _acceleration * Time.deltaTime;
            transform.position += direction * _currentSpeed * GameManager.SpeedMultiplier * Time.deltaTime;
        }

        private void Rotate()
        {
            transform.rotation *= Quaternion.Euler(0f,_rotationSpeed * Time.deltaTime,0f);
        }

        public void Enable()
        {
            gameObject.SetActive(true);
        }

        public void Disable()
        {
            IsMagnetized = false;
            gameObject.SetActive(false);
        }

        public bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }

        private void OnMoveBack(float value)
        {
            transform.Translate(0f,0f, -value, Space.World);
        }

        private void OnGameEnd()
        {
            if (IsActive())
            {
                Disable();
            }
        }
    }
}