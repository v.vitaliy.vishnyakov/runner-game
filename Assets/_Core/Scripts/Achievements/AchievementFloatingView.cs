﻿using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class AchievementFloatingView: MonoBehaviour
    {
        private readonly int AnimationCompletedTriggerHash = Animator.StringToHash("AnimationCompleted");
        [SerializeField] private Animator _animator;
        [SerializeField] private Image _image;
        [SerializeField] private Text _title;

        public void InitializeAndShow(AchievementModel achievementModel)
        {
            _image.sprite = achievementModel.image;
            _title.text = achievementModel.title;
            _animator.SetTrigger(AnimationCompletedTriggerHash);
        }
    }
}