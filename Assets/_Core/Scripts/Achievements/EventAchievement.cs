﻿namespace _Core
{
    public class EventAchievement : Achievement
    {
        public override bool CheckForCompletion(AchievementCompletionData achData)
        {
            if (!_isCompleted)
            {
                _isCompleted = achData.eventValue;
                if (_isCompleted)
                {
                    OnCompletion();
                }
            }
            return _isCompleted;
        }

        public override void InitializeAchievementView(AchievementView achievementView)
        {
            if (_achievementView != null) return;
            achievementView.Initialize(_model, _isCompleted, OnViewEnable);
            _achievementView = achievementView;
        }

        private void OnViewEnable()
        {
            
        }
    }
}