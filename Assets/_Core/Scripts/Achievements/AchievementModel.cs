﻿using UnityEngine;

namespace _Core
{
    public abstract class AchievementModel : ScriptableObject
    {
        public string title;
        public string description;
        public int id;
        public int rewardSize;
        public Sprite image;
        public Sprite rewardImage;
    }
}