﻿namespace _Core
{
    public class AchievementHandlersModel
    {
        public AchievementHandler coinsAchievementHandler;
        public AchievementHandler distanceAchievementHandler;
        public AchievementHandler uniqueEventAchievementHandler;

        public AchievementHandlersModel()
        {
            coinsAchievementHandler = new AchievementHandler();
            distanceAchievementHandler = new AchievementHandler();
            uniqueEventAchievementHandler = new AchievementHandler();
        }
    }
}