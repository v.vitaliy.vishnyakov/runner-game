﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "DistanceAchievement", menuName = "Achievements/Distance Achievement", order = 0)]
    public class DistanceAchievementModel : ProgressAchievementModel
    {
        
    }
}