﻿using System.Collections.Generic;

namespace _Core
{
    public class AchievementHandler
    {
        private List<Achievement> _achievements = new List<Achievement>();
        private List<Achievement> _completedAchievements = new List<Achievement>();

        public bool IsEmpty => _achievements.Count == 0;
        public List<Achievement> ActiveAchievements => _achievements;
        public List<Achievement> CompletedAchievements => _completedAchievements;

        public void AddAchievement(Achievement achievement)
        {
            if (!achievement.IsCompleted)
            {
                _achievements.Add(achievement);
            }
            else
            {
                _completedAchievements.Add(achievement);
            }
        }
        
        public void CheckCompletion(AchievementCompletionData achData)
        {
            if (IsEmpty) return;
            for (int i = _achievements.Count-1; i >= 0; i--)
            {
                if (_achievements[i].CheckForCompletion(achData))
                {
                    _completedAchievements.Add(_achievements[i]);
                    _achievements.RemoveAt(i);
                }
            }
        }
    }
}