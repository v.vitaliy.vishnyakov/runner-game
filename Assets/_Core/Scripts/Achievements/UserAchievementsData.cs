﻿using System;
using System.Collections.Generic;

namespace _Core
{
    [Serializable]
    public class UserAchievementsData
    {
        public List<int> completedAchievementsIds = new List<int>();

        public void AddAchievementToCompleted(Achievement achievement)
        {
            completedAchievementsIds.Add(achievement.Model.id);
        }
    }
}