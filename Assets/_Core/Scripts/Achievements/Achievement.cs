﻿using System;

namespace _Core
{
    public abstract class Achievement
    {
        private event Action<Achievement> _onCompletion;
        protected bool _isCompleted;
        protected AchievementModel _model;
        protected AchievementView _achievementView;
        public int Id => _model.id;
        public bool IsCompleted => _isCompleted;
        public AchievementModel Model => _model;
        

        public abstract bool CheckForCompletion(AchievementCompletionData achData);
        public abstract void InitializeAchievementView(AchievementView achievementView);
        
        public void Initialize(AchievementModel achModel, Action<Achievement> onCompletion)
        {
            _model = achModel;
            _onCompletion += onCompletion;
        }
        
        public void SetCompletion(bool value)
        {
            _isCompleted = value;
        }
        
        protected virtual void OnCompletion()
        {
            _onCompletion?.Invoke(this);
            if (_achievementView != null)
            {
                _achievementView.MakeCompleted(true);    
            }
        }
    }
}