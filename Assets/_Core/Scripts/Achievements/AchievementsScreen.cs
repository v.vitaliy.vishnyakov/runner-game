﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class AchievementsScreen : Screen
    {
        [SerializeField] private LayoutGroup _layoutGroup;
        [SerializeField] private Button _closeButton;

        private void Awake()
        {
            _closeButton.onClick.AddListener(Hide);
        }

        public void AddAchievementViews(List<AchievementView> achievementViews)
        {
            foreach (AchievementView achievementView in achievementViews)
            {
                achievementView.SetParent(_layoutGroup.transform);
            }
        }
    }
}