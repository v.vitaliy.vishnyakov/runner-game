﻿namespace _Core
{
    public class ProgressAchievement : Achievement
    {
        public int ProgressValue { get; private set; }
        public ProgressAchievementModel progressModel => (ProgressAchievementModel) _model;

        public override bool CheckForCompletion(AchievementCompletionData achData)
        {
            if (!_isCompleted)
            {
                ProgressValue = achData.progressValue;
                _isCompleted =  ProgressValue >= progressModel.goal;
                if (_isCompleted)
                {
                    OnCompletion();
                }
            }
            return _isCompleted;
        }

        public override void InitializeAchievementView(AchievementView achievementView)
        {
            if (_achievementView != null) return;
            achievementView.Initialize(_model, _isCompleted, OnViewEnable);
            achievementView.SetProgressValue(ProgressValue, (progressModel.goal));
            _achievementView = achievementView;
        }

        private void OnViewEnable()
        {
            _achievementView.SetProgressValue(ProgressValue, progressModel.goal);
        }
    }
}