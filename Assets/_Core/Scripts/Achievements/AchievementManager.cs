﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace _Core
{
    public class AchievementManager : Singleton<AchievementManager>
    {
        private const string UserAchievementDataKey = "UserAchData";
        
        [SerializeField] private List<AchievementModel> _achievementsModels;
        [SerializeField] private AchievementFloatingView achievementFloatingView;
        [SerializeField] private AchievementView _achievementViewPrefab;
        [SerializeField] private string _soundOnCompletion;

        private AchievementHandlersModel _achievementHandlers = new AchievementHandlersModel();
        private UserAchievementsData _userAchData;
        private List<Achievement> _completedAchs = new List<Achievement>();
        
        public static List<Achievement> CompletedAchievements => _instance._completedAchs;

        protected override void Initialize()
        {
            SubscribeHandlersToStatsEvents();
        }
        
        private void SubscribeHandlersToStatsEvents()
        {
            UserStatsActionModel userStatsActionModel = new UserStatsActionModel()
            {
                onCoinCollected = OnCoinCollected,
                onDistanceRun = OnDistanceRun,
            };
            UserManager.SubscribeToStatsEvents(userStatsActionModel);
        }
        
        private void Start()
        {
            InitializeAchievements();
            UserManager.InvokeAllDataEvents();
            CreateAchievementViews(_achievementHandlers.coinsAchievementHandler);
            CreateAchievementViews(_achievementHandlers.distanceAchievementHandler);
            CreateAchievementViews(_achievementHandlers.uniqueEventAchievementHandler);
        }

        private void InitializeAchievements()
        {
            _userAchData = LoadUserAchievementData();
            AchievementInitializer achievementInitializer = new AchievementInitializer();
            achievementInitializer.InitializeAchievements(_achievementsModels, _achievementHandlers, _userAchData, OnAchievementCompletion);
        }

        private void CreateAchievementViews(AchievementHandler achievementHandler)
        {
            List<AchievementView> achievementViewList = new List<AchievementView>();
            foreach (Achievement ach in achievementHandler.ActiveAchievements.Concat(achievementHandler.CompletedAchievements))
            {
                AchievementView achievementView = Instantiate(_achievementViewPrefab);
                ach.InitializeAchievementView(achievementView);
                achievementViewList.Add(achievementView);
            }
            UIManager.GetScreen<AchievementsScreen>().AddAchievementViews(achievementViewList);
        }

        private void SaveUserAchievementData(UserAchievementsData userAchData)
        {
            SaveManager.Save(UserAchievementDataKey, userAchData);
        }

        private UserAchievementsData LoadUserAchievementData()
        {
            UserAchievementsData userAchData = new UserAchievementsData();
            // UserAchievementsData userAchData = SaveManager.Load<UserAchievementsData>(UserAchievementDataKey);
            // if (userAchData == null)
            // {
            //     userAchData = new UserAchievementsData();
            // }
            return userAchData;
        }
        
        public static void OnAchievementCompletion(Achievement achievement)
        {
            UserManager.AddCoin(achievement.Model.rewardSize);
            _instance.achievementFloatingView.InitializeAndShow(achievement.Model);
            SoundManager.PlaySFX("AchDrop");
            _instance._completedAchs.Add(achievement);
            _instance._userAchData.AddAchievementToCompleted(achievement);
            _instance.SaveUserAchievementData(_instance._userAchData);
        }
        
        private void OnCoinCollected(Statistics statisticsData)
        {
            AchievementCompletionData achData = new AchievementCompletionData
            {
                progressValue = statisticsData.global.coinsCollected
            };
            _achievementHandlers.coinsAchievementHandler.CheckCompletion(achData);
        }

        private void OnDistanceRun(Statistics statisticsData)
        {
            AchievementCompletionData achData = new AchievementCompletionData
            {
                progressValue = (int)statisticsData.global.distancePassed
            };
            _achievementHandlers.distanceAchievementHandler.CheckCompletion(achData);
        }
    }
}