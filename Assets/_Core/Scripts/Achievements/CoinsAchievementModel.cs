﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "CoinsAchievement", menuName = "Achievements/Coins Achievement", order = 0)]
    public class CoinsAchievementModel : ProgressAchievementModel
    {
    }
}