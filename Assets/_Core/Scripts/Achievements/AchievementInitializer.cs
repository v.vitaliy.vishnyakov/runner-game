﻿using System;
using System.Collections.Generic;

namespace _Core
{
    public class AchievementInitializer
    {
        private List<AchievementModel> _achievementsModels;
        private UserAchievementsData _userAchievementsData;
        
        public void InitializeAchievements(
            List<AchievementModel> achievements,
            AchievementHandlersModel achievementHandlers,
            UserAchievementsData userAchData,
            Action<Achievement> onCompletion)
        {
            _achievementsModels = achievements;
            _userAchievementsData = userAchData;

            InitializeAchievements<CoinsAchievementModel, ProgressAchievement>(achievementHandlers.coinsAchievementHandler, onCompletion);
            InitializeAchievements<DistanceAchievementModel, ProgressAchievement>(achievementHandlers.distanceAchievementHandler, onCompletion);
        }

        private void InitializeAchievements<T, U>(AchievementHandler achHandler, Action<Achievement> onCompletion)
            where T : AchievementModel where U : Achievement, new()
        {
            List<AchievementModel> achModels = _achievementsModels.FindAll(a => a is T);
            foreach (AchievementModel achModel in achModels)
            {
                U achievement = new U();
                achievement.Initialize((T) achModel, onCompletion);
                DefineAchievementCompletion(achievement);
                achHandler.AddAchievement(achievement);
            }
        }

        private void DefineAchievementCompletion(Achievement achievement)
        {
            achievement.SetCompletion(_userAchievementsData.completedAchievementsIds.Contains(achievement.Model.id));
            if (achievement.IsCompleted)
            {
                AchievementManager.CompletedAchievements.Add(achievement);
            }
        }
    }
}