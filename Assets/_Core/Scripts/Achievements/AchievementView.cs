﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class AchievementView : MonoBehaviour
    {
        private event Action Enable;
        [SerializeField] private Image _image;
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _description;
        [SerializeField] private Image _rewardImage;
        [SerializeField] private TextMeshProUGUI _rewardSize;
        [SerializeField] private Image _filter;
        [SerializeField] private Image _completionSign;
        [SerializeField] private TextMeshProUGUI _progressValueText;
        public bool IsCompleted { get; private set; }

        public void Initialize(AchievementModel achievementModel, bool isCompleted, Action onEnable)
        {
            _image.sprite = achievementModel.image;
            _title.text = achievementModel.title;
            _description.text = achievementModel.description;
            _rewardImage.sprite = achievementModel.rewardImage;
            _rewardSize.text = achievementModel.rewardSize.ToString();
            _progressValueText.text = String.Empty;
            MakeCompleted(isCompleted);
            Enable += onEnable;
        }

        public void MakeCompleted(bool value)
        {
            _completionSign.gameObject.SetActive(value);
            _filter.gameObject.SetActive(value);
            _progressValueText.gameObject.SetActive(!value);
            _rewardImage.gameObject.SetActive(!value);
            _rewardSize.gameObject.SetActive(!value);
            IsCompleted = value;
        }
        
        public void SetParent(Transform parentTransform)
        {
            transform.SetParent(parentTransform);
        }

        public void SetProgressValue(int progressValue, int goalValue)
        {
            _progressValueText.text = progressValue + "/" + goalValue;
        }

        private void OnEnable()
        {
            Enable?.Invoke();
        }
    }
}