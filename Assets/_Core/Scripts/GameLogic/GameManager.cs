﻿using System;
using UnityEngine;

namespace _Core
{
    public class GameManager : Singleton<GameManager>
    {
        private event Action<float> OnMoveBack;
        private event Action OnGameEnd;
        
        [Header("Game speed")] 
        [SerializeField] private float _animationSpeedDivider;
        [SerializeField] private float _speedMultiplier;
        [SerializeField] private bool _useSpeedIncrement;
        [SerializeField] private SpeedIncrement _speedIncrement;
        [Header("References")] 
        [SerializeField] private PlayerController _playerController;
        [SerializeField] private Material _characterSkinMaterial;
        [SerializeField] private Camera _playerCamera;
        [SerializeField] private Camera _menuCamera;
        [SerializeField] private GameObject _playerStartSpot;
        [SerializeField] private TrackPart _initialTrackPart;
        [SerializeField] private int _moveBackDistance;
        [SerializeField] private TrackCleaner _trackCleaner;
        [SerializeField] private string _musicKey;
        private float _initialSpeedMultiplierValue;
        private float _scoreDistance;
        private float _distance;
        private Vector3 _playerLastPosition;
        

        public static Vector3 CharacterPosition => _instance._playerController.GetCharacterPosition();

        private bool _isGameRunning;
        public static float SpeedMultiplier => _instance._speedMultiplier;
        public static float ScoreDistanceMultiplier { get; private set; }
        public static float AnimationSpeedMultiplier { get => 1 + (SpeedMultiplier - 1) / _instance._animationSpeedDivider; }
        
        protected override void Initialize()
        {
            _initialSpeedMultiplierValue = _speedMultiplier;
        }

        private void Update()
        {
            if (_isGameRunning)
            {
                CalculateScoreDistance();
                if (_useSpeedIncrement)
                {
                    IncrementSpeed();
                }
            }
        }

        private void Start()
        {
            SoundManager.PlayMusic(_musicKey);
        }

        public static void StartGame()
        {
            _instance._isGameRunning = true;
            _instance._menuCamera.gameObject.SetActive(false);
            UIManager.HideScreen<MainMenuScreen>();
            UIManager.ShowHUD();
            _instance._speedMultiplier = _instance._initialSpeedMultiplierValue;
            TrackManager.StartTrackGeneration();
            _instance._playerController.Run();
            _instance._scoreDistance = 0;
            ScoreDistanceMultiplier = 1;
            _instance._playerLastPosition = _instance._playerController.transform.position;
            UserManager.ResetLastGameProgress();
        }

        private void CalculateScoreDistance()
        {
            float _distancePassedByFrame = _playerController.transform.position.z - _playerLastPosition.z;
            _distance += _distancePassedByFrame;
            _scoreDistance += _distancePassedByFrame * ScoreDistanceMultiplier;
            if (_scoreDistance >= 1)
            {
                UserManager.AddDistanceRun(_scoreDistance);
                _scoreDistance = 0;
            }

            if (_distance >= _moveBackDistance)
            {
                _trackCleaner.gameObject.SetActive(false);
                OnMoveBack?.Invoke(_distance);
                _trackCleaner.gameObject.SetActive(true);
                _distance = 0;
            }
            _playerLastPosition = _playerController.transform.position;
        }

        public static void EndGame()
        {
            _instance._isGameRunning = false;
            UIManager.HideHUD();
            UIManager.ShowScreen<EndgameScreen>();
        }

        public static void GoToMenu()
        {
            _instance._playerController.Reset();
            _instance._playerController.transform.position = _instance._playerStartSpot.transform.position;
            _instance._menuCamera.gameObject.SetActive(true);
            TrackManager.EndTrackGeneration();
            TrackManager.DeleteTrack();
            UIManager.HideScreen<EndgameScreen>();
            UIManager.ShowScreen<MainMenuScreen>();
            _instance.OnGameEnd?.Invoke();
            _instance._initialTrackPart.gameObject.SetActive(true);
        }
        
        private void IncrementSpeed()
        {
            _speedMultiplier += _speedIncrement.value * (Time.deltaTime / _speedIncrement.period);
        }

        public static void SetCharacterSkin(Texture2D characterSkinTexture)
        {
            _instance._characterSkinMaterial.mainTexture = characterSkinTexture;
        }

        public static void SetScoreDistanceMultiplier(float value)
        {
            ScoreDistanceMultiplier = value;
        }
        public static void ResetScoreDistanceMultiplier()
        {
            ScoreDistanceMultiplier = 1;
        }

        public static void SubscribeToGameEvents(Action<float> onMoveBackHandler, Action onEndGameHandler)
        {
            _instance.OnMoveBack += onMoveBackHandler;
            _instance.OnGameEnd += onEndGameHandler;
        }
    }
}