﻿using System;

namespace _Core
{
    [Serializable]
    public class UpgradeStep
    {
        public int cost;
        public float value;
    }
}