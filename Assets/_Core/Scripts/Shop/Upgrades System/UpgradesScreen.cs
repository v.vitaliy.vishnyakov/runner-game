﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class UpgradesScreen : Screen
    {
        [SerializeField] private TextMeshProUGUI _creditsAmountLabel;
        [SerializeField] private LayoutGroup _layoutGroup;
        [SerializeField] private Button _backButton;
        
        private void Awake()
        {
            _backButton.onClick.AddListener(Hide);
        }

        private void Update()
        {
            _creditsAmountLabel.text = UserManager.UserData.coins.ToString();
        }

        public void SetUpgradeViews(List<UpgradeView> shopUpgradeViews)
        {
            foreach (UpgradeView shopUpgradeView in shopUpgradeViews)
            {
                shopUpgradeView.SetParent(_layoutGroup.transform);
            }
        }
        
        private void OnEnable()
        {
            UpdateCreditsLabel();
        }
        
        private void UpdateCreditsLabel()
        {
            
        }
    }
}