﻿using System.Collections.Generic;
using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "ShopUpgrade", menuName = "Shop/Shop Upgrade", order = 0)]
    public class UpgradeModel : ScriptableObject
    {
        public int id;
        public string title;
        public string description;
        public Sprite image;
        public List<UpgradeStep> upgradeSteps;
    }
}