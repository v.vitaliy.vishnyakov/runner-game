﻿using System;

namespace _Core
{
    public class UpgradeSubscribeArgs
    {
        public int upgradeId;
        public float initialValue;
        public Action<float> onUpgrade;

        public UpgradeSubscribeArgs(int upgradeId, float initialValue, Action<float> onUpgradeAction)
        {
            this.upgradeId = upgradeId;
            this.initialValue = initialValue;
            this.onUpgrade = onUpgradeAction;
        }
    }
}