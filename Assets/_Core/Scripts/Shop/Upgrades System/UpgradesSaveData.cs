﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _Core
{
    [Serializable]
    public class UpgradeSaveData
    {
        public int id;
        public int stepsPurchased;

        public UpgradeSaveData(int id, int stepsPurchased)
        {
            this.id = id;
            this.stepsPurchased = stepsPurchased;
        }
    }
    
    [Serializable]
    public class UpgradesSaveData
    {
        public List<UpgradeSaveData> list = new List<UpgradeSaveData>();

        public void SaveData(int id, int stepsPurchased)
        {
            UpgradeSaveData upgradeSaveData = list.FirstOrDefault(x => x.id == id);
            if (upgradeSaveData == null)
            {
                upgradeSaveData = new UpgradeSaveData(id, stepsPurchased);
                list.Add(upgradeSaveData);
            }
            else
            {
                upgradeSaveData.stepsPurchased = stepsPurchased;
            }
        }
    }
}