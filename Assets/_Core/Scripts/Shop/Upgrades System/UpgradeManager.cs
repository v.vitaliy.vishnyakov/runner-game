﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace _Core
{
    public class UpgradeManager : Singleton<UpgradeManager>
    {
        private const string UpgradesProgressDataKey = "UpgradesData";
        
        [SerializeField] private List<UpgradeModel> _upgradesModels;
        [Header("References")]
        [SerializeField] private UpgradeView upgradeViewPrefab;
        
        private List<UpgradeController> _upgradeControllers = new List<UpgradeController>();
        private UpgradesSaveData _upgradesSaveData;

        protected override void Initialize()
        {
            CreateUpgradeControllers();
        }

        private void CreateUpgradeControllers()
        {
            foreach (UpgradeModel shopUpgrade in _upgradesModels)
            {
                UpgradeController upgradeController = new UpgradeController(shopUpgrade, TryBuyUpgrade);
                upgradeController.SubscribeToUpgrade(OnUpgrade);
                _upgradeControllers.Add(upgradeController);
            }
        }

        private void Start()
        {
            LoadUpgradesData();
            InitializeUpgradesScreen();
        }

        private void InitializeUpgradesScreen()
        {
            List<UpgradeView> upgradeViews = new List<UpgradeView>();
            foreach (UpgradeController upgradeController in _upgradeControllers)
            {
                UpgradeView upgradeView = Instantiate(upgradeViewPrefab);
                upgradeController.InitializeUpgradeView(upgradeView);
                upgradeViews.Add(upgradeView);
            }
            UIManager.GetScreen<UpgradesScreen>().SetUpgradeViews(upgradeViews);
        }

        private bool TryBuyUpgrade(int cost)
        {
            return UserManager.TrySpendCoins(cost);
        }
        
        public static void SubscribeToUpgrades(List<UpgradeSubscribeArgs> subscribeArgsList)
        {
            foreach (UpgradeSubscribeArgs subscribeArgs in subscribeArgsList)
            {
                UpgradeController upgradeController = _instance._upgradeControllers.FirstOrDefault(x => x.Id == subscribeArgs.upgradeId);
                upgradeController?.SubscribeOnUpgrade(subscribeArgs.onUpgrade, subscribeArgs.initialValue);
            }
        }

        private void LoadUpgradesData()
        {
            _upgradesSaveData = new UpgradesSaveData();
            //_upgradesSaveData = SaveManager.Load<UpgradesSaveData>(UpgradesProgressDataKey) ?? new UpgradesSaveData();
            foreach (UpgradeSaveData upgradeSaveData in _upgradesSaveData.list)
            {
                UpgradeController upgradeController = _upgradeControllers.Find(x => x.Id == upgradeSaveData.id);
                upgradeController.SetStepsPurchased(upgradeSaveData.stepsPurchased);
            }
        }

        private void SaveUpgradesData()
        {
            SaveManager.Save(UpgradesProgressDataKey, _upgradesSaveData);
        }

        private void OnUpgrade(UpgradeController upgradeController)
        {
            _upgradesSaveData.SaveData(upgradeController.Id, upgradeController.StepsPurchased);
            SaveUpgradesData();
        }
    }
}