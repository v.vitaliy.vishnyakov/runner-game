﻿using System;
using System.Globalization;

namespace _Core
{
    public class UpgradeController
    {
        private event Action<UpgradeController> OnUpgrade;
        
        private Action<float> _onUpgrade;
        private Func<int, bool> _onBuyTry;
        private UpgradeModel _upgradeModel;
        private UpgradeView _upgradeView;
        private int _stepsPurchased;
        public int Id => _upgradeModel.id;
        public int StepsPurchased => _stepsPurchased;

        public UpgradeController(UpgradeModel upgradeModel, Func<int, bool> onBuyTry)
        {
            _upgradeModel = upgradeModel;
            _onBuyTry = onBuyTry;
        }

        public void SubscribeOnUpgrade(Action<float> onUpgrade, float initialValue)
        {
            _onUpgrade = onUpgrade;
            _upgradeView.SetInitialCellValueText(initialValue.ToString(CultureInfo.CurrentCulture));
        }

        public void SetStepsPurchased(int stepsPurchased)
        {
            _stepsPurchased = stepsPurchased;
        }

        public void InitializeUpgradeView(UpgradeView upgradeView)
        {
            if (_upgradeView == null)
            {
                upgradeView.InitializeByUpgrade(_upgradeModel, _stepsPurchased);
                upgradeView.SubscribeToUpgradeButtonClicked(OnBuy);
                _upgradeView = upgradeView;
            }
        }
        
        private void OnBuy()
        {
            if (_onBuyTry(_upgradeModel.upgradeSteps[_stepsPurchased].cost))
            {
                ApplyUpgrade();
            }
        }

        private void ApplyUpgrade()
        {
            UpdateView();
            _onUpgrade(_upgradeModel.upgradeSteps[_stepsPurchased].value);
            _stepsPurchased++;
            OnUpgrade?.Invoke(this);
        }

        private void UpdateView()
        {
            string cellValueText = _upgradeModel.upgradeSteps[_stepsPurchased].value.ToString(CultureInfo.InvariantCulture);
            string buttonCostText = null;
            if (_stepsPurchased + 1 != _upgradeModel.upgradeSteps.Count)
            {
                buttonCostText = _upgradeModel.upgradeSteps[_stepsPurchased + 1].cost.ToString();
            }
            _upgradeView.UpdateView(cellValueText, buttonCostText); 
        }

        public void SubscribeToUpgrade(Action<UpgradeController> onUpgrade)
        {
            OnUpgrade += onUpgrade;
        }
    }
}