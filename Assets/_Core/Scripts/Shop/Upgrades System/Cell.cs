﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    [RequireComponent(typeof(Image))]
    public class Cell: MonoBehaviour
    {
        [SerializeField] private Image _imageComponent;
        [SerializeField] private TextMeshProUGUI _valueText;

        public void SetColor(Color color)
        {
            _imageComponent.color = color;
        }

        public void SetValueText(string valueText)
        {
            _valueText.text = valueText;
        }
    }
}