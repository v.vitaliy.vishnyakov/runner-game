﻿using System;
using System.Collections.Generic;
using System.Globalization;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class UpgradeView : MonoBehaviour
    {
        private event Action UpgradeButtonClicked;
        
        [Header("References")]
        [SerializeField] private Image _image;
        [SerializeField] private TextMeshProUGUI _description;
        [SerializeField] private RectTransform _cellsArea;
        [SerializeField] private MyButton _buyButton;
        [SerializeField] private Cell _cellPrefab;
        [Header("Parameters")]
        [SerializeField] private Color _cellPurchased;
        [SerializeField] private Color _cellDefault;
        [SerializeField] private string _fullUpgradeButtonText;
        
        private int _stepsPurchased;
        private List<Cell> _cells = new List<Cell>();
        
        private int MaxStepsNumber => _cells.Count;

        private void Awake()
        {
            Initialize();
        }
        private void Initialize()
        {
            _buyButton.SubscribeOnClick(OnUpgradeButtonClick);
        }

        public void InitializeByUpgrade(UpgradeModel upgradeModel, int stepsPurchased)
        {
            _stepsPurchased = stepsPurchased;
            CreateCells(stepsPurchased, upgradeModel);
            SetupVisuals(stepsPurchased, upgradeModel);
        }

        private void CreateCells(int stepsPurchased, UpgradeModel upgradeModel)
        {
            Cell initialCell = Instantiate(_cellPrefab, _cellsArea);
            initialCell.SetColor(_cellPurchased);
            _cells.Add(initialCell);
            
            for (int i = 0; i < upgradeModel.upgradeSteps.Count; i++)
            {
                Cell cell = Instantiate(_cellPrefab, _cellsArea);
                cell.SetColor(i < stepsPurchased ? _cellPurchased : _cellDefault);
                if (i == stepsPurchased - 1)
                {
                    cell.SetValueText(upgradeModel.upgradeSteps[stepsPurchased-1].value.ToString(CultureInfo.CurrentCulture));
                }
                _cells.Add(cell);
            }            
        }

        private void SetupVisuals(int stepsPurchased, UpgradeModel upgradeModel)
        {
            _image.sprite = upgradeModel.image;
            _description.text = upgradeModel.description;
            if (stepsPurchased < MaxStepsNumber - 1)
            {
                _buyButton.GetComponentInChildren<TextMeshProUGUI>().text = upgradeModel.upgradeSteps[stepsPurchased].cost.ToString();
            }
            else
            {
                _buyButton.GetComponentInChildren<TextMeshProUGUI>().text = _fullUpgradeButtonText;
            }
        }


        public void UpdateView(string cellValueText, [CanBeNull] string buttonCostText)
        {
            _cells[_stepsPurchased].SetValueText(String.Empty);
            _stepsPurchased++;
            _cells[_stepsPurchased].SetColor(_cellPurchased);
            _cells[_stepsPurchased].SetValueText(cellValueText);
            
            if (buttonCostText != null)
            {
                _buyButton.GetComponentInChildren<TextMeshProUGUI>().text = buttonCostText;
            }
            else
            {
                _buyButton.GetComponentInChildren<TextMeshProUGUI>().text = _fullUpgradeButtonText;
                _buyButton.DisableButton();
            }
        }

        public void SetParent(Transform parentTransform)
        {
            transform.SetParent(parentTransform);
        }
        public void SetInitialCellValueText(string initialValue)
        {
            if (_stepsPurchased != 0) return;
            _cells[0].SetValueText(initialValue);
        }

        private void OnUpgradeButtonClick()
        {
            UpgradeButtonClicked?.Invoke();
        }

        public void SubscribeToUpgradeButtonClicked(Action onButtonClicked)
        {
            UpgradeButtonClicked += onButtonClicked;
        }
    }
}