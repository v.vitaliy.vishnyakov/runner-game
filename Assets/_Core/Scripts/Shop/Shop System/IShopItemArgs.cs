﻿namespace _Core
{
    public interface IShopItemArgs
    {
        public int Id { get; }
        public int Price { get; }
    }
}