﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class ShopItemView : MonoBehaviour, IPoolableObject
    {
        private event Action OnItemPurchased;

        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private Image _image;
        [SerializeField] private MyButton _buyButton;
        [SerializeField] private MyToggle _chooseToggle;
        
        private Action<IShopItemArgs> _onChooseCallback;
        private Func<IShopItemArgs, bool> _onBuyCallback;
        private ShopItemArgs _shopItemArgs; 
        
        private void Awake()
        {
            Initialize();
        }

        private void Initialize()
        {
            _buyButton.SubscribeOnClick(OnBuyButtonClick);
            //_chooseToggle.onValueChanged.AddListener(OnChooseToggleValueChanged);
        }

        public void InitializeByItem(ShopItemModel shopItemModel, bool isPurchased, bool isChosen, Func<IShopItemArgs, bool> onBuyCallback, Action<IShopItemArgs> onChooseCallback)
        {
            _shopItemArgs = new ShopItemArgs(shopItemModel);
            _title.text = shopItemModel.Name;
            _image.sprite = shopItemModel.Image;
            _buyButton.GetComponentInChildren<TextMeshProUGUI>().text = shopItemModel.Price.ToString();
            _onChooseCallback = onChooseCallback;
            _onBuyCallback = onBuyCallback;
            MakeChoosable(isPurchased);
            _chooseToggle.Initialize(isChosen, OnChooseToggleValueChanged);
        }

        private void OnBuyButtonClick()
        {
            if (!_onBuyCallback(_shopItemArgs)) return;
            MakeChoosable(true);
            OnItemPurchased?.Invoke();
        }

        private void OnChooseToggleValueChanged(bool value)
        {
            if (value)
            {
                _onChooseCallback(_shopItemArgs);    
            }
        }

        public void AddToToggleGroup(ToggleGroup toggleGroup)
        {
            _chooseToggle.SetToggleGroup(toggleGroup);
        }
        
        private void MakeChoosable(bool value)
        {
            _buyButton.gameObject.SetActive(!value);
            _chooseToggle.gameObject.SetActive(value);
        }
        
        public void SetParent(Transform parentTransform)
        {
            transform.SetParent(parentTransform);
        }

        public void SubscribeToOnItemPurchased(Action onItemPurchasedHandler)
        {
            OnItemPurchased += onItemPurchasedHandler;
        }
        
        public void Enable()
        {
            gameObject.SetActive(true);
            
        }
        
        public void Disable()
        {
            gameObject.SetActive(false);
        }

        public bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }
    }
}