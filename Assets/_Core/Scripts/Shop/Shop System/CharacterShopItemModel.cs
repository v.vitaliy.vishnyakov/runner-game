﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "CharacterShopItemModel", menuName = "Shop/Character Shop Item Model", order = 0)]
    public class CharacterShopItemModel : ShopItemModel
    {
        [SerializeField] private int _id;
        [SerializeField] private int _price;
        [SerializeField] private string _name;
        [SerializeField] private Sprite _image;
        [SerializeField] private CharacterItemData characterItemData;

        public override int Id => _id;
        public override int Price => _price;
        public override string Name => _name;
        public override Sprite Image => _image;
        public override IShopItemData Data => characterItemData;
    }
}