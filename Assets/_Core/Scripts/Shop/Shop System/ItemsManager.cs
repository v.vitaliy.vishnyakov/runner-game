﻿using System.Collections.Generic;
using UnityEngine;

namespace _Core
{
    public class ItemsManager : Singleton<ItemsManager>
    {
        private const string ItemsDataKey = "ItemsDataKey";
        private const string ItemsStorageFileName = "ItemsStorage";
        
        [SerializeField] private int _itemByDefaultId;
        [SerializeField] private ShopItemView _shopItemViewPrefab;
        private ItemsData _itemsData;
        private ItemsStorage _itemsStorage;

        protected override void Initialize() { }

        private void Start()
        {
            LoadFullItemsData();
            ApplyLastChosenItem();
            InitializeShopScreen();
        }
        private void LoadFullItemsData()
        {
            _itemsData = LoadItemsData();
            _itemsStorage = Resources.Load<ItemsStorage>(ItemsStorageFileName);
        }
        private void ApplyLastChosenItem()
        {
            ApplyItem(_itemsData._chosenItemId);
        }
        
        private void InitializeShopScreen()
        {
            List<ShopItemView> shopItemViews = new List<ShopItemView>();
            foreach (ShopItemModel shopItem in _itemsStorage.Items)
            {
                ShopItemView shopItemView = Instantiate(_shopItemViewPrefab);
                bool isItemPurchased = IsItemPurchased(shopItem.Id);
                bool isItemChosen = _itemsData._chosenItemId == shopItem.Id;;
                shopItemView.InitializeByItem(shopItem, isItemPurchased, isItemChosen, OnBuyingItem, OnChoosingItem);
                shopItemViews.Add(shopItemView);
            }
            UIManager.GetScreen<ShopScreen>().InitializeScreen(shopItemViews);
        }
        
        private static void ApplyItem(int itemId)
        {
            ShopItemModel shopItemModel = _instance._itemsStorage.Items.Find(x => x.Id == itemId);
            if (shopItemModel is CharacterShopItemModel characterItemModel)
            {
                if (characterItemModel.Data is CharacterItemData characterItemData)
                {
                    GameManager.SetCharacterSkin(characterItemData.characterSkinTexture);
                }
            }
            _instance._itemsData._chosenItemId = itemId;
            _instance.SaveItemsData();
        }

        private static bool IsItemPurchased(int itemId)
        {
            return _instance._itemsData._purchasedItemsIds.Contains(itemId);
        }
        
        private void OnChoosingItem(IShopItemArgs itemArgs)
        {
            ApplyItem(itemArgs.Id);
        }

        private bool OnBuyingItem(IShopItemArgs itemArgs)
        {
            if (!UserManager.TrySpendCoins(itemArgs.Price)) return false;
            _instance._itemsData._purchasedItemsIds.Add(itemArgs.Id);
            _instance.SaveItemsData();
            return true;
        }
        
        private ItemsData LoadItemsData()
        {
            ItemsData loadedItemsData = SaveManager.Load<ItemsData>(ItemsDataKey) ?? new ItemsData
            {
                _purchasedItemsIds = new List<int>(new int[]{_itemByDefaultId}),
                _chosenItemId = _itemByDefaultId
            };
            return loadedItemsData;
        }

        private void SaveItemsData()
        {
            SaveManager.Save(ItemsDataKey, _instance._itemsData);
        }

        public static void DeleteItemsData()
        {
            SaveManager.Delete<ItemsData>(ItemsDataKey);
            _instance.Initialize();
        }
    }
}