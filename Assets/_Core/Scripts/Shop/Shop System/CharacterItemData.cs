﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "CharacterItemData", menuName = "Shop/Character Item Data", order = 0)]
    public class CharacterItemData : ScriptableObject, IShopItemData
    {
        public Texture2D characterSkinTexture;
    }
}