﻿using System.Collections.Generic;
using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "ItemsStorage", menuName = "Shop/Items Storage", order = 1)]
    public class ItemsStorage : ScriptableObject
    {
        [SerializeField] private List<ShopItemModel> items;
        
        public List<ShopItemModel> Items => items;
    }
}