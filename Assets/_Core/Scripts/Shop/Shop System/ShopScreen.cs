﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class ShopScreen : Screen
    {
        [Header("UI Elements")]
        [SerializeField] private TextMeshProUGUI _creditsAmountLabel;
        [SerializeField] private LayoutGroup _layoutGroup;
        [SerializeField] private ToggleGroup _toggleGroup;
        [SerializeField] private Button _backButton;
        
        public void InitializeScreen(List<ShopItemView> shopItemViews)
        {
            foreach (ShopItemView itemView in shopItemViews)
            {
                itemView.SubscribeToOnItemPurchased(OnItemPurchased);
                itemView.SetParent(_layoutGroup.transform);
                itemView.AddToToggleGroup(_toggleGroup);
            }
        }

        private void Awake()
        {
            _backButton.onClick.AddListener(Hide);
        }

        private void OnEnable()
        {
            UpdateCreditsLabel();
        }

        private void OnItemPurchased()
        {
            UpdateCreditsLabel();
        }

        private void UpdateCreditsLabel()
        {
            _creditsAmountLabel.text = UserManager.UserData.coins.ToString();
        }
        
    }
}