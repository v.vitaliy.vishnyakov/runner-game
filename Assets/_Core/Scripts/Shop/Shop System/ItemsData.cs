﻿using System;
using System.Collections.Generic;

namespace _Core
{
    [Serializable]
    public class ItemsData
    {
        public int _chosenItemId;
        public List<int> _purchasedItemsIds;
        
        public ItemsData() {}

        public ItemsData(List<int> purchasedItemsIds, int chosenItemId)
        {
            _purchasedItemsIds = purchasedItemsIds;
            _chosenItemId = chosenItemId;
        }
    }
}