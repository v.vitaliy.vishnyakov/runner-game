﻿using UnityEngine;

namespace _Core
{
    public abstract class ShopItemModel : ScriptableObject
    {
        public abstract int Id { get; }
        public abstract int Price { get; }
        public abstract string Name { get; }
        public abstract Sprite Image { get; }
        public abstract IShopItemData Data { get; }
    }
}