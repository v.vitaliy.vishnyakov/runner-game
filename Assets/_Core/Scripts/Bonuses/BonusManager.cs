﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace _Core
{
    [Serializable]
    public class BonusUpgradesIds
    {
        public int magnetHandlerId;
        public int shieldHandlerId;
    }
    public class BonusManager : Singleton<BonusManager>
    {
        private const string BonusUpgradesDataKey = "BonusData";
        [SerializeField] private BonusesParameters _parameters;
        [SerializeField] private PlayerController _playerController;
        [SerializeField] private BonusUpgradesIds bonusUpgradesIds;
        private BonusUpgradesData _bonusUpgradesData;
        private Dictionary<Type, BonusStatus> _bonusStats;
        
        public static BonusesParameters Parameters => _instance._parameters;
        public static BonusUpgradesData BonusUpgradesData => _instance._bonusUpgradesData;
           
        protected override void Initialize()
        {
            _bonusStats = new Dictionary<Type, BonusStatus>();
        }

        private void Start()
        {
            LoadBonusUpgradesData();
            InitializeBonusStats();
            SubscribeOnUpgrades();
        }

        private void SubscribeOnUpgrades()
        {
            List<UpgradeSubscribeArgs> upgradeActions = new List<UpgradeSubscribeArgs>()
            {
                new UpgradeSubscribeArgs(
                    bonusUpgradesIds.magnetHandlerId,
                    _parameters.magnetInitialLifeTime,
                    (value) =>
                    {
                        _bonusUpgradesData.upgradedMagnetLifetime = value;
                        SaveBonusUpgradesData();
                    }),
                new UpgradeSubscribeArgs(
                    bonusUpgradesIds.shieldHandlerId,
                    _parameters.shieldInitialLifeTime,
                    (value ) =>
                    {
                        _bonusUpgradesData.upgradedShieldLifetime = value;
                        SaveBonusUpgradesData();
                    })
            };
            UpgradeManager.SubscribeToUpgrades(upgradeActions);
        }

        private void InitializeBonusStats()
        {
            foreach (Bonus bonus in SpawnManager.BonusesList)
            {
                _bonusStats.Add(bonus.GetType(), new BonusStatus(false));
            }
        }

        public static void HandleBonus(Bonus bonus)
        {
            SoundManager.PlaySFX(_instance._parameters._soundOnPickupKey);
            _instance.StartCoroutine(_instance.BonusEffectRoutine(bonus, _instance.GetBonusStatus(bonus)));
        }

        public static Bonus GetRandomAvailableBonus()
        {
            List<Type> availableBonuses = new List<Type>();
            foreach (var bonusStatus in _instance._bonusStats)
            {
                if (bonusStatus.Value.isActive == false)
                {
                    availableBonuses.Add(bonusStatus.Key);
                }
            }
            if (availableBonuses.Count == 0)
            {
                return null;
            }
            int bonusIndex = Random.Range(0, availableBonuses.Count);
            Type selectedBonusType = availableBonuses[bonusIndex];
            return SpawnManager.SpawnBonus(selectedBonusType);
        }

        private IEnumerator BonusEffectRoutine(Bonus bonus, BonusStatus bonusStatus)
        {
            bonusStatus.isActive = true;
            bonus.ApplyEffect(_playerController);
            UIManager.AddBonusToHud(bonus.image, bonus.LifeTime);
            yield return new WaitForSeconds(bonus.LifeTime);
            bonusStatus.isActive = false;
            bonus.DiscardEffect(_playerController);
        }

        private BonusStatus GetBonusStatus(Bonus bonus)
        {
            return _bonusStats[bonus.GetType()];
        }

        private void LoadBonusUpgradesData()
        {
            _bonusUpgradesData = SaveManager.Load<BonusUpgradesData>(BonusUpgradesDataKey);
            if (_bonusUpgradesData is null)
            {
                _bonusUpgradesData = new BonusUpgradesData
                {
                    upgradedMagnetLifetime = _parameters.magnetInitialLifeTime,
                    upgradedShieldLifetime = _parameters.shieldInitialLifeTime,
                    upgradedScoreBoostLifetime = _parameters.scoreBoostInitialLifeTime
                };
            }
        }

        private void SaveBonusUpgradesData()
        {
            SaveManager.Save(BonusUpgradesDataKey, _bonusUpgradesData);
        }
    }
}