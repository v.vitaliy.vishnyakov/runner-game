﻿using UnityEngine;

namespace _Core
{
    [CreateAssetMenu(fileName = "BonusesParameters", menuName = "Parameters", order = 0)]
    public class BonusesParameters : ScriptableObject
    {
        public float magnetRadius;
        public float scoreBoostValue;
        public float magnetInitialLifeTime;
        public float scoreBoostInitialLifeTime;
        public float shieldInitialLifeTime;
        public string _soundOnPickupKey;
    }
}