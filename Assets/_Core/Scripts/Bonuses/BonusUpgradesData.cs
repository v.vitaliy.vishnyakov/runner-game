﻿using System;

namespace _Core
{
    [Serializable]
    public class BonusUpgradesData
    {
        public float upgradedMagnetLifetime;
        public float upgradedScoreBoostLifetime;
        public float upgradedShieldLifetime;
    }
}