﻿namespace _Core
{
    public class Shield : Bonus
    {
        public override float LifeTime => BonusManager.BonusUpgradesData.upgradedShieldLifetime;
        
        public override void ApplyEffect(PlayerController playerController)
        {
            playerController.IsShieldOn = true;
        }

        public override void DiscardEffect(PlayerController playerController)
        {
            playerController.IsShieldOn = false;
        }
    }
}