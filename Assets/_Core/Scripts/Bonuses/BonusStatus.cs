﻿namespace _Core
{
    public class BonusStatus
    {
        public bool isActive { get; set; }

        public BonusStatus(bool isActive)
        {
            this.isActive = isActive;
        }
    }
}