﻿using UnityEngine;

namespace _Core
{
    [RequireComponent(typeof(SphereCollider))]
    public class MagnetBehaviour : MonoBehaviour
    {
        [SerializeField] private SphereCollider _collider;
        [SerializeField] private LayerMask _coinLayer;

        private void Awake()
        {
            Disable();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (Utils.CheckCollision(other, _coinLayer))
            {
                other.GetComponent<Coin>().IsMagnetized = true;
            }
        }

        public void Enable(float radius)
        {
            SetRadius(radius);
            gameObject.SetActive(true);
        }

        public void Disable()
        {
            gameObject.SetActive(false);
        }

        private void SetRadius(float radius)
        {
            _collider.radius = radius;
        }
    }
}