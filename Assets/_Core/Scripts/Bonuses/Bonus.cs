﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    [RequireComponent(typeof(Collider))]
    public abstract class Bonus : MonoBehaviour, IPoolableObject
    {
        public Sprite image;
        public abstract float LifeTime { get; }

        public abstract void ApplyEffect(PlayerController playerController);     // called (by BonusManager) when Player collides with this bonus 
        public abstract void DiscardEffect(PlayerController playerController);   // called (by BonusManager) in LifeTime seconds after ApplyEffect

        private void Start()
        {
            GameManager.SubscribeToGameEvents(OnMoveBack, OnGameEnd);
        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag(Tags.Player))
            {
                BonusManager.HandleBonus(this);
                Disable();
            }
        }

        public void Enable()
        {
            gameObject.SetActive(true);
        }

        public void Disable()
        {
            gameObject.SetActive(false);
        }

        public bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }
        
        private void OnMoveBack(float value)
        {
            transform.Translate(0,0, -value, Space.World);
        }

        private void OnGameEnd()
        {
            if (IsActive())
            {
                Disable();
            }
        }
    }
}