﻿namespace _Core
{
    public class Magnet : Bonus
    {
        public override float LifeTime => BonusManager.BonusUpgradesData.upgradedMagnetLifetime;

        public override void ApplyEffect(PlayerController playerController)
        {
            playerController.EnableMagnet();
        }
        public override void DiscardEffect(PlayerController playerController)
        {
            playerController.DisableMagnet();
        }
    }
}