﻿namespace _Core
{
    public class ScoreBoost : Bonus
    {
        public override float LifeTime => BonusManager.BonusUpgradesData.upgradedScoreBoostLifetime;

        public override void ApplyEffect(PlayerController playerController)
        {
            GameManager.SetScoreDistanceMultiplier(BonusManager.Parameters.scoreBoostValue);
        }

        public override void DiscardEffect(PlayerController playerController)
        {
            GameManager.ResetScoreDistanceMultiplier();   
        }
    }
}