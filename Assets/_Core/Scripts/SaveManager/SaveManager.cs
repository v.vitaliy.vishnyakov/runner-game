﻿namespace _Core
{
    public class SaveManager : Singleton<SaveManager>
    {
        private ISaveController _saveController = new PlayerPrefsSaveController();

        protected override void Initialize() {}

        public static void Save<T>(string key, T data)
        {
            _instance._saveController.Save(key, data);
        }
        public static T Load<T>(string key)
        {
            T smth = _instance._saveController.Load<T>(key);
            return smth;
        }
        public static void Delete<T>(string key)
        {
            _instance._saveController.Delete<T>(key);
        }
    }
}