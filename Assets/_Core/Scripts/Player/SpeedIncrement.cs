﻿using System;

namespace _Core
{
    [Serializable]
    public struct SpeedIncrement
    {
        public float value;
        public float period;
    }
}