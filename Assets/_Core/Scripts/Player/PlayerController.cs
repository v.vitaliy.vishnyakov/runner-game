﻿using System;
using UnityEditor.Animations;
using UnityEngine;

namespace _Core
{
    public class PlayerController : MonoBehaviour
    {
        [Header("Animation")] 
        [SerializeField] private AnimatorController _playerAnimatorController;
        [Header("Parameters")]
        [SerializeField] private PlayerParameters _parameters;
        [Header("References")]
        [SerializeField] private PlayerCollider _playerCollider;
        [SerializeField] private GroundChecker _groundChecker;
        [SerializeField] private MagnetBehaviour _magnet;
        [SerializeField] private Character _character;
        [SerializeField] private TrackCleaner _trackCleaner;
        private bool _isRunning;
        private bool _isJumping;
        private bool _isSliding;
        private float _verticalVelocity;
        private float _slidedDistance;
        private float _distancePassedByFrame;
        private Vector3 _lastPosition;
        private float _initialJumpAnimSpeed;
        private float _initialSlideAnimSpeed;
        private float _initialRunAnimSpeed;

        public bool IsHit { get; private set; }
        public Line CurrentLine { get; private set; }
        public bool IsShieldOn { get; set; }


        private void Awake()
        {
            Initialize();
            _trackCleaner.gameObject.SetActive(false);
            GameManager.SubscribeToGameEvents(OnMoveBack, null);
        }
        
        private void OnMoveBack(float value)
        {
            transform.Translate(0,0, -value, Space.World);
        }

        private void Initialize()
        {
            _playerCollider.OnHit = OnHit;
            _character.SetAnimatorController(_playerAnimatorController);
            _initialJumpAnimSpeed = _character.Animator.GetFloat(PlayerHashes.fJumpAnimationSpeed);
            _initialRunAnimSpeed = _character.Animator.GetFloat(PlayerHashes.fRunAnimationSpeed);
            _initialSlideAnimSpeed = _character.Animator.GetFloat(PlayerHashes.fSlideAnimationSpeed);
        }

        public void Run()
        {
            if (!_isRunning)
            {
                _isRunning = true;
                _character.Animator.SetBool(PlayerHashes.isRunning, true);
                _trackCleaner.gameObject.SetActive(true);
            }
        }

        private void StopRunning()
        {
            if (_isRunning)
            {
                _isRunning = false;
                _character.Animator.SetBool(PlayerHashes.isRunning, false);    
            }
        }

        private void Update()
        {
            if (_isRunning)
            {
                ApplyGameSpeed();
                Move();
                CalculatePassedDistance();
                CheckAnimationsStates();
                HandleInput();
                ApplyVerticalVelocity();
            }
        }
        
        public void EnableMagnet()
        {
            _magnet.Enable(BonusManager.Parameters.magnetRadius);
            _magnet.transform.SetParent(_character.gameObject.transform);
            _magnet.transform.localPosition = Vector3.zero;
        }

        public void DisableMagnet()
        {
            _magnet.Disable();
            _magnet.transform.parent = this.gameObject.transform;
        }

        private void Move()
        {
            if (_isRunning)
            {
                float speed = _parameters.initialSpeed * GameManager.SpeedMultiplier;
                transform.position += transform.forward * speed * Time.deltaTime;
            }   
        }
        
        private void CheckAnimationsStates()
        {
            if (_isJumping && _groundChecker.IsGrounded)
            {
                Land();
            }
            if (_isSliding)
            {
                _slidedDistance += _distancePassedByFrame;
                if (_slidedDistance >= _parameters.slideLength)
                {
                    StopSliding();
                }
            }
        }

        private void HandleInput()
        {
            // temporary controls - change on Swipes at InputSystem implementation
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                Slide();
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                Dash(Line.Left);
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                Dash(Line.Right);
            }
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                Jump();
            }
        }
        
        private void Dash(Line lineToDashTo)
        {
            if (CurrentLine != lineToDashTo)
            {
                TranslateCharacter(new Vector3((int)lineToDashTo * _parameters.dashDistance, 0,0));
                if (CurrentLine == Line.Middle)
                {
                    CurrentLine = lineToDashTo;
                }
                else
                {
                    CurrentLine = Line.Middle;
                }
            } 
        }
        
        private void Slide()
        {
            if (!_isSliding)
            {
                if (_isJumping)
                {
                    StopJumping();
                }
                _isSliding = true;
                float animationSpeed = _initialSlideAnimSpeed * GameManager.AnimationSpeedMultiplier;
                _character.Animator.SetFloat(PlayerHashes.fSlideAnimationSpeed,animationSpeed);
                _character.Animator.SetBool(PlayerHashes.isSliding, true);
                _playerCollider.SetSlidingCollider();
            }
        }
        private void StopSliding()
        {
            _isSliding = false;
            _slidedDistance = 0;
            _character.Animator.SetBool(PlayerHashes.isSliding, false);
            _playerCollider.SetDefaultCollider();
        }
        
        private void Jump()
        {
            if (!_isJumping && _groundChecker.IsGrounded)
            {
                if (_isSliding)
                {
                    StopSliding();
                }
                _isJumping = true;
                float animationSpeed = _initialJumpAnimSpeed * GameManager.AnimationSpeedMultiplier;
                _character.Animator.SetFloat(PlayerHashes.fJumpAnimationSpeed, animationSpeed);
                _character.Animator.SetBool(PlayerHashes.isJumping, true);
                _verticalVelocity = _parameters.jumpForce;
            }
        }
        private void StopJumping()
        {
            _isJumping = false;
            _character.Animator.SetBool(PlayerHashes.isJumping, false);
            _verticalVelocity = -1 * _parameters.jumpInterruptionForce;
        }
        private void Land()
        {
            _isJumping = false;
            _character.Animator.SetBool(PlayerHashes.isJumping, false);
        }
        
        private void ApplyVerticalVelocity()
        {
            if (!_groundChecker.IsGrounded)
            {
                _verticalVelocity -= _parameters.gravity * Time.deltaTime;
            }
            if (_groundChecker.IsGrounded && _verticalVelocity <= 0)
            {
                _verticalVelocity = 0;
            }
            float deltaHeight = _verticalVelocity * Time.deltaTime -
                                (_parameters.gravity * Mathf.Pow(Time.deltaTime, 2) / 2);
            TranslateCharacter(new Vector3(0, deltaHeight, 0) );
        }
        
        private void CalculatePassedDistance()
        {
            _distancePassedByFrame = (transform.position - _lastPosition).magnitude;
            _lastPosition = transform.position;
        }
        

        private void OnHit()
        {
            if (!IsShieldOn)
            {
                IsHit = true;
                StopRunning();
                if (_isJumping)
                {
                    StopJumping();
                }
                if (_isSliding)
                {
                    StopSliding();
                }
                _character.Animator.SetTrigger(PlayerHashes.hitTrigger);
                GameManager.EndGame();
            }
            else
            {
                IsShieldOn = false;
            }
        }

        public void TranslateCharacter(Vector3 position)
        {
            _groundChecker.transform.Translate(position);
            _playerCollider.transform.Translate(position);
            _character.transform.Translate(position);
        }
        
        public void ApplyGameSpeed()
        {
            float animationSpeed = _initialRunAnimSpeed * GameManager.AnimationSpeedMultiplier;
            _character.Animator.SetFloat(PlayerHashes.fRunAnimationSpeed, animationSpeed);
        }

        public Vector3 GetCharacterPosition()
        {
            if (_character == null) return Vector3.zero;
            return _character.transform.position;
        }

        public void Reset()
        {
            _character.Animator.Play("Idle");
            CurrentLine = Line.Middle;
            _trackCleaner.gameObject.SetActive(false);
            _playerCollider.transform.localPosition = Vector3.zero;
            _character.transform.localPosition = Vector3.zero;
            _groundChecker.transform.localPosition = new Vector3(0, 2, 0);
        }
    }
}