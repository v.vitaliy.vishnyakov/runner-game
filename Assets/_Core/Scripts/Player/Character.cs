﻿using UnityEditor.Animations;
using UnityEngine;

namespace _Core
{
    [RequireComponent(typeof(Animator))]
    public class Character : MonoBehaviour
    {
        private Animator _animator;
        public bool IsInitialized { get; private set; }
        public Animator Animator => _animator;
        
        private void Awake()
        {
            Initialize();
        }
        private void Initialize()
        {
            _animator = GetComponent<Animator>();
            _animator.applyRootMotion = false;
        }
        public void SetAnimatorController(AnimatorController _animatorController)
        {
            _animator.runtimeAnimatorController = _animatorController;
            IsInitialized = true;
        }
    }
}