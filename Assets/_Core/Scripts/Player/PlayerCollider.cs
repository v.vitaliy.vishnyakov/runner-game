﻿using System;
using UnityEngine;

namespace _Core
{
    public class PlayerCollider : MonoBehaviour
    {
        [Header("Parameters")]
        [SerializeField] private Vector3 _slidingColliderSize;
        [SerializeField] private Vector3 _slidingColliderCenter;
        [SerializeField] private LayerMask _hitMask;
        [SerializeField] private string _hitTag;
        [Header("References")] 
        [SerializeField] private BoxCollider _collider;
        
        private Vector3 _defaultColliderSize;
        private Vector3 _defaultColliderCenter;
        private Action _onHitCallback;
        public bool IsSlidingCollider {  get; private set; }
        public Action OnHit { set => _onHitCallback = value; }
        public PlayerController PlayerController => transform.parent.GetComponent<PlayerController>();
        
        private void OnTriggerEnter(Collider other)
        {
            if (Utils.CheckCollision(other, _hitMask))
            {
                _onHitCallback.Invoke();
            }
        }

        private void Awake()
        {
            Initialize();
        }

        private void Initialize()
        {
            _defaultColliderSize = _collider.size;
            _defaultColliderCenter = _collider.center;
            IsSlidingCollider = false;
        }
        
        public void SetSlidingCollider()
        {
            if (!IsSlidingCollider)
            {
                _collider.size = _slidingColliderSize;
                _collider.center = _slidingColliderCenter;
                IsSlidingCollider = true;    
            }
        }
        public void SetDefaultCollider()
        {
            if (IsSlidingCollider)
            {
                _collider.center = _defaultColliderCenter;
                _collider.size = _defaultColliderSize;
                IsSlidingCollider = false;    
            }
        }
    }
}