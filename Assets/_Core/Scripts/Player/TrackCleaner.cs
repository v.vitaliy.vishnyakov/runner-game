﻿using UnityEngine;

namespace _Core
{
    [RequireComponent(typeof(Collider))]
    public class TrackCleaner : MonoBehaviour
    {
        [SerializeField] private LayerMask _destroyMask;

        private void OnTriggerExit(Collider other)
        {
            if (Utils.CheckCollision(other, _destroyMask))
            {
                if (other.gameObject.TryGetComponent<IPoolableObject>(out var obj))
                {
                    obj.Disable();    
                }
            }
        }
    }
}