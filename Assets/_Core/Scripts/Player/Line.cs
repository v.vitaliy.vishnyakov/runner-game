﻿namespace _Core
{
    public enum Line
    {
        Left = -1,
        Middle = 0,
        Right = 1
    }
}