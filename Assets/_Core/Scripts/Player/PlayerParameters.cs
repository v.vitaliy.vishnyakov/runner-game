﻿namespace _Core
{
    [System.Serializable]
    public class PlayerParameters
    {
        public float gravity;
        public float initialSpeed;
        public float jumpForce;
        public float jumpInterruptionForce;
        public float dashDistance;
        public float slideLength;
        
        
    }
}