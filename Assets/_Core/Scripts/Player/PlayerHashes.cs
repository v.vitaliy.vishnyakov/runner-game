﻿using System;
using UnityEngine;

namespace _Core
{
    [Serializable]
    public class PlayerHashes
    {
        public static readonly int isRunning = Animator.StringToHash("isRunning");
        public static readonly int isSliding = Animator.StringToHash("isSliding");
        public static readonly int isJumping = Animator.StringToHash("isJumping");
        public static readonly int hitTrigger = Animator.StringToHash("Hit");
        public static readonly int reviveTrigger = Animator.StringToHash("Revive");
        public static readonly int fSlideAnimationSpeed = Animator.StringToHash("SlideSpeed");
        public static readonly int fJumpAnimationSpeed = Animator.StringToHash("JumpSpeed");
        public static readonly int fRunAnimationSpeed = Animator.StringToHash("RunSpeed");
    }
}