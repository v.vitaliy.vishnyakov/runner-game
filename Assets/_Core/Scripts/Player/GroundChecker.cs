﻿using UnityEngine;

namespace _Core
{
    // GroundChecker must be located on the very top of Character and Collider
    public class GroundChecker : MonoBehaviour
    {
        private const float _rayLength = 20f;
        [Header("Parameters")]
        [SerializeField] private LayerMask _groundMask;
        [Header("References")]
        [SerializeField] private PlayerController _player;
        private float _characterHeight;
        private RaycastHit hit;
        public bool IsGrounded { get; private set; }
        public Vector3 GroundProjection => hit.point;

        private void Awake()
        {
            Initialize();
        }

        private void Initialize()
        {
            IsGrounded = true;
            _characterHeight = transform.localPosition.y;
        }

        void Update()
        {
            IsGrounded = CheckForGround();
            if (IsCharacterFallBelowGround())
            {
                CorrectCharacterPosition();
            }
        }

        private bool CheckForGround()
        {
            bool result = Physics.Raycast(transform.position, Vector3.down, out hit, _rayLength, _groundMask);
            if (result)
            {
                return !(hit.distance > _characterHeight);
            }
            return false;
        }
        
        private bool IsCharacterFallBelowGround()
        {
            return IsGrounded && hit.point.y >= _player.GetCharacterPosition().y;
        }

        private void CorrectCharacterPosition()
        {
            float offset = Vector3.Distance(hit.point, _player.GetCharacterPosition());
            _player.TranslateCharacter(new Vector3(0,offset,0));
        }
    }
}