﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

namespace _Core
{
    public class SpawnManager : Singleton<SpawnManager>
    {
        private delegate bool Condition<in T>(T obj);
        [SerializeField] private SpawnPrefabs _prefabs;
        private List<TrackPart> _trackPartsPool= new List<TrackPart>();
        private List<Coin> _coinsPool= new List<Coin>();
        private List<Bonus> _bonusPool= new List<Bonus>();
        private List<Sound> _soundsPool = new List<Sound>();
        
        public static int TrackPartsCount => _instance._prefabs.trackParts.Count;
        public static List<TrackPart> TrackParts => _instance._prefabs.trackParts;
        public static List<Bonus> BonusesList => _instance._prefabs.bonuses;

        
        protected override void Initialize()
        { }

        public static TrackPart SpawnTrackPart(Vector3 position, int index)
        {
            return _instance.Spawn(position, _instance._trackPartsPool,
                _instance._prefabs.trackParts[index], part => part.Id == _instance._prefabs.trackParts[index].Id);
        }

        public static Coin SpawnCoin(Vector3 position)
        {
            return _instance.Spawn(position, _instance._coinsPool, _instance._prefabs.coin);
        }
        
        public static Bonus SpawnBonus(Type bonusType)
        {
            return _instance.Spawn(Vector3.zero, _instance._bonusPool, _instance.GetBonusPrefab(bonusType), bonus => bonus.GetType() == bonusType);
        }
        
        public static Sound SpawnSound()
        {
            return _instance.Spawn(Vector2.zero, _instance._soundsPool, _instance._prefabs.soundPrefab);
        }

        private Bonus GetBonusPrefab(Type bonusType)
        {
            return _instance._prefabs.bonuses.FirstOrDefault(bonus => bonus.GetType() == bonusType);
        }

        private T Spawn<T, U>(Vector3 position, List<T> pool, T prefab, Condition<T> condition = null) 
            where T: MonoBehaviour, IPoolableObject
            where U: T
        {
            T obj = null;
            foreach (var poolObj in pool)
            {
                if (poolObj is U && !poolObj.gameObject.activeInHierarchy)
                {
                    if (condition != null)
                    {
                        if (!condition(poolObj))
                        {
                            continue;
                        }
                    }
                    obj = poolObj;
                    break;
                }
            }
            if (obj == null)
            {
                obj = Instantiate(prefab);
                pool.Add(obj);
            }
            obj.gameObject.transform.position = position;
            obj.Enable();
            return obj;       
        }
        private T Spawn<T>(Vector3 position, List<T> pool, T prefab, Condition<T> condition = null) where T : MonoBehaviour, IPoolableObject
        {
            return Spawn<T, T>(position, pool, prefab, condition);
        }
        
        public static void DestroyObject(IPoolableObject gameObject)
        {
            gameObject.Disable();
        }
        
        public static void DestroyAllObjects()
        {
            DisableAllObjectsInPool(_instance._trackPartsPool);
            DisableAllObjectsInPool(_instance._coinsPool);
            DisableAllObjectsInPool(_instance._bonusPool);
        }
        
        private static void DisableAllObjectsInPool<T>(List<T> pool) where T: IPoolableObject
        {
            foreach (var obj in pool)
            {
                if (obj.IsActive())
                {
                    obj.Disable();
                }
            }
        }
    }
}