﻿using System;
using System.Collections.Generic;

namespace _Core
{
    [Serializable]
    public class SpawnPrefabs
    {
        public List<TrackPart> trackParts;
        public List<Bonus> bonuses;
        public Coin coin;
        public Sound soundPrefab;
    }
}