﻿using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class MainMenuScreen : Screen
    {
        [SerializeField] private Button _playButton;
        [SerializeField] private MyButton _shopButton;
        [SerializeField] private MyButton _upgradesButton;
        [SerializeField] private MyButton _achievementsButton;
        [SerializeField] private MyButton _settingsButton;

        private void Awake()
        {
            _playButton.onClick.AddListener(OnPlayButtonClick);
            _shopButton.SubscribeOnClick(OnShopButtonClick);
            _upgradesButton.SubscribeOnClick(OnUpgradesButtonClick);
            _achievementsButton.SubscribeOnClick(OnAchievementsButtonClick);
            _settingsButton.SubscribeOnClick(OnSettingsButtonClick);
        }

        private void OnPlayButtonClick()
        {
            GameManager.StartGame();
        }
        
        private void OnShopButtonClick()
        {
            UIManager.ShowScreen<ShopScreen>();
        }
        private void OnUpgradesButtonClick()
        {
            UIManager.ShowScreen<UpgradesScreen>();
        }
        private void OnAchievementsButtonClick()
        {
            UIManager.ShowScreen<AchievementsScreen>();
        }
        private void OnSettingsButtonClick()
        {
            //UIManager.ShowScreen<>();
        }
    }
}