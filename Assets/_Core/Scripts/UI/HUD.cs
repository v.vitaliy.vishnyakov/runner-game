﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class HUD : Screen
    {
        [SerializeField] private TextMeshProUGUI _scoreDistanceLabel;
        [SerializeField] private TextMeshProUGUI _coinsCountLabel;
        [SerializeField] private RectTransform _bonusArea;

        private List<Image> _bonusImages = new List<Image>();

        public void SetScoreDistance(int scoreDistance)
        {
            _scoreDistanceLabel.text = scoreDistance.ToString();
        }
        
        public void SetCoinsCount(int coinsCount)
        {
            _coinsCountLabel.text = coinsCount.ToString();
        }

        public void AddBonusImage(Sprite bonusImage, float lifeTimeSeconds)
        {
            StartCoroutine(BonusImage(bonusImage, lifeTimeSeconds));
        }

        private IEnumerator BonusImage(Sprite bonusImage, float lifeTimeSeconds)
        {
            GameObject obj = new GameObject();
            obj.AddComponent<Image>();
            obj.GetComponent<Image>().sprite = bonusImage;
            obj.transform.SetParent(_bonusArea);
            yield return new WaitForSeconds(lifeTimeSeconds);
            Destroy(obj);
        }
    }
}