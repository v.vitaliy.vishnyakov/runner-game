﻿using UnityEngine;

namespace _Core
{
    public abstract class Screen : MonoBehaviour
    {
        public bool IsShown => gameObject.activeInHierarchy;
        
        public void Show(IScreenParameters screenParameters = null)
        {
            if (screenParameters != null)
            {
                ApplyScreenParameters(screenParameters);
            }
            gameObject.SetActive(true);
        }

        protected virtual void ApplyScreenParameters(IScreenParameters screenParameters)
        {
            
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}