﻿namespace _Core
{
    public class EndgameScreenParameters : IScreenParameters
    {
        public readonly int finalScore;
        public readonly int coinsCollected;

        public EndgameScreenParameters(int finalScore, int coinsCollected)
        {
            this.finalScore = finalScore;
            this.coinsCollected = coinsCollected;
        }
    }
}