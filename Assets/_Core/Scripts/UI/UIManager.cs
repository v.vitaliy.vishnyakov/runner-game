﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace _Core
{
    public class UIManager : Singleton<UIManager>
    {
        [SerializeField] private List<Screen> _screens;
        [SerializeField] private HUD _hud;
        
        protected override void Initialize()
        { }

        private void Start()
        {
            SubscribeOnUserStatsEvents();
        }

        private void SubscribeOnUserStatsEvents()
        {
            UserStatsActionModel userStatsActionModel = new UserStatsActionModel
            {
                onCoinCollected = OnCoinCollectedHandler,
                onDistanceRun = OnDistanceRunHandler
            };
            UserManager.SubscribeToStatsEvents(userStatsActionModel);
        }
        
        private void OnCoinCollectedHandler(Statistics statisticsData)
        {
            _hud.SetCoinsCount(statisticsData.lastRun.coinsCollected);
        }
        
        private void OnDistanceRunHandler(Statistics statisticsData)
        {
            _hud.SetScoreDistance((int)statisticsData.lastRun.distancePassed);
        }

        public static void AddBonusToHud(Sprite image, float bonusLifeTime)
        {
            _instance._hud.AddBonusImage(image, bonusLifeTime);
        }

        public static void ClearHUD()
        {
            _instance._hud.SetCoinsCount(0);
            _instance._hud.SetScoreDistance(0);
        }

        public static void ShowHUD()
        {
            ClearHUD();
            _instance._hud.Show();
        }
        
        public static void HideHUD()
        {
            _instance._hud.Hide();
        }

        public static void ShowScreen<T>() where T: Screen
        {
            Screen screen = GetScreen<T>();
            if (screen is EndgameScreen endgameScreen)
            {
                EndgameScreenParameters screenParameters = new EndgameScreenParameters
                    ((int)UserManager.StatisticsData.lastRun.distancePassed,
                        UserManager.StatisticsData.lastRun.coinsCollected);
                endgameScreen.Show(screenParameters);
                return;
            }
            screen.Show();
        }

        public static void HideScreen<T>() where T: Screen
        {
            Screen screen =  GetScreen<T>();
            screen.Hide();
        }
        
        public static T GetScreen<T>() where T : Screen
        {
            foreach (Screen screen in _instance._screens)
            {
                if (screen is T)
                {
                    return screen as T;
                }
            }
            return null;
        }
    }
}