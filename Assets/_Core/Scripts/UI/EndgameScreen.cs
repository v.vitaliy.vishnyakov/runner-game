﻿using TMPro;
using UnityEngine;

namespace _Core
{
    public class EndgameScreen : Screen
    {
        [SerializeField] private TextMeshProUGUI _scoreLabel;
        [SerializeField] private TextMeshProUGUI _coinsLabel;
        [SerializeField] private MyButton _menuButton;

        private void Awake()
        {
            _menuButton.SubscribeOnClick(OnMenuButtonClick);
        }

        protected override void ApplyScreenParameters(IScreenParameters screenParameters)
        {
            if (screenParameters is EndgameScreenParameters endgameScreenParameters)
            {
                _scoreLabel.text = endgameScreenParameters.finalScore.ToString();
                _coinsLabel.text = endgameScreenParameters.coinsCollected.ToString();
            }
        }
        
        private void OnMenuButtonClick()
        {
            GameManager.GoToMenu();
        }
    }
}