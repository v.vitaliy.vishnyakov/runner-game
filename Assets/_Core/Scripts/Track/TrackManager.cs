﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Core
{
    public class TrackManager : Singleton<TrackManager>
    {
        [SerializeField] private int _minTracksCountAMoment;
        [SerializeField] private int _samePartsDelay;
        [SerializeField] [Range(0,1)] private float _initialBonusSpawnChance;
        [SerializeField] [Range(0,1)] private float _bonusSpawnChanceIncrementStep;
        
        private float _bonusSpawnChance;
        
        private TrackPart _lastTrackPart;
        private List<TrackPart> _currentTrackParts;
        private List<int> _excludedPartsIndexes;
        private List<int> _partsIndexes;

        public static bool IsTrackGenerationOn { get; set; }

        private void Start()
        {
            GameManager.SubscribeToGameEvents(OnMoveBack, OnGameEnd);
        }

        protected override void Initialize()
        {
            _bonusSpawnChance = _initialBonusSpawnChance;
            _currentTrackParts = new List<TrackPart>();
            if (_samePartsDelay > SpawnManager.TrackPartsCount - 1)
            {
                _samePartsDelay = SpawnManager.TrackPartsCount - 1;
            }
            _excludedPartsIndexes = new List<int>();
            _partsIndexes = Enumerable.Range(0, SpawnManager.TrackPartsCount).ToList();
        }
        
        private void OnTrackPartDisable()
        {
            if (IsTrackGenerationOn)
            {
                _currentTrackParts.RemoveAt(0);
                if (_currentTrackParts.Count < _minTracksCountAMoment)
                {
                    AddTrackParts(_minTracksCountAMoment - _currentTrackParts.Count);
                }
            }
        }

        public static void AddTrackParts(int number)
        {
            for (int i = 0; i < number; i++)
            {
                int index = _instance.GetNextPartIndex();
                TrackPart trackPart = SpawnManager.SpawnTrackPart(Vector3.zero, index);
                _instance._excludedPartsIndexes.Add(index);
                _instance.CheckIndexes();
                if (_instance._lastTrackPart == null)
                {
                    trackPart.transform.position =Vector3.zero;
                    _instance._lastTrackPart = trackPart;
                }
                else
                {
                    trackPart.transform.position = _instance.CalculateNextTrackPartPosition(trackPart);
                    _instance._lastTrackPart = trackPart;
                }
                _instance._currentTrackParts.Add(trackPart);
                Bonus bonusToSpawn = _instance.GetBonus();
                trackPart.Initialize(bonusToSpawn);
                trackPart.SubscribeOnTrackDisable(_instance.OnTrackPartDisable);
            }
        }

        private Bonus GetBonus()
        {
            Bonus bonus = null;
            _instance._bonusSpawnChance += _instance._bonusSpawnChanceIncrementStep;
            if (Random.value <= _instance._bonusSpawnChance)
            {
                bonus = BonusManager.GetRandomAvailableBonus();
                _instance._bonusSpawnChance = _instance._initialBonusSpawnChance;
            }
            return bonus;
        }
        
        private int GetNextPartIndex()
        {
            List<int> allowedIndexes = _partsIndexes.Where(x => !_excludedPartsIndexes.Contains(x)).ToList();
            return allowedIndexes[Random.Range(0, allowedIndexes.Count)];
        }

        private void CheckIndexes()
        {
            if (_excludedPartsIndexes.Count > _samePartsDelay)
            {
                _excludedPartsIndexes.RemoveAt(0);
            }
        }

        private Vector3 CalculateNextTrackPartPosition(TrackPart nextTrackPart)
        {
            return new Vector3(_lastTrackPart.transform.position.x, _lastTrackPart.transform.position.y,
                _lastTrackPart.transform.position.z +_lastTrackPart.Length/2 + nextTrackPart.Length/2);
        }

        public static void StartTrackGeneration()
        {
            _instance._lastTrackPart = null;
            AddTrackParts(_instance._minTracksCountAMoment);
            IsTrackGenerationOn = true;
        }

        public static void EndTrackGeneration()
        {
            IsTrackGenerationOn = false;
            _instance._excludedPartsIndexes.Clear();
        }

        public static void DeleteTrack()
        {
            foreach (TrackPart trackPart in _instance._currentTrackParts)
            {
                trackPart.Disable();
            }
            _instance._currentTrackParts.Clear();
        }

        public static Vector3 GetTrackPartPosition(int trackIndex)
        {
            if (_instance._currentTrackParts.Count > trackIndex)
            {
                return _instance._currentTrackParts[trackIndex].transform.position;
            }
            return _instance._currentTrackParts[_instance._currentTrackParts.Count-1].transform.position;
        }
        
        private void OnMoveBack(float value)
        {
            foreach (TrackPart trackPart in _currentTrackParts)
            {
                trackPart.transform.Translate(0,0,-value, Space.World);
            }
        }

        private void OnGameEnd()
        {
            DeleteTrack();
        }
        
    }
}