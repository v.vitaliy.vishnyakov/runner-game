﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Core
{
    public class TrackPart : MonoBehaviour, IPoolableObject
    {
        private event Action OnTrackDisable;
        
        [SerializeField] private float _length;
        [SerializeField] private int _id;
        [SerializeField] private List<GameObject> _coinSpots;
        [SerializeField] private GameObject _bonusSpot;

        public float Length => _length;
        public int Id => _id;
        public bool IsOnDisableSubscribed => OnTrackDisable == null;

        public void Initialize(Bonus bonusToSpawn = null)
        {
            foreach (GameObject coinSpot in _coinSpots)
            {
                SpawnManager.SpawnCoin(coinSpot.transform.position);
            }
            if (bonusToSpawn != null)
            {
                bonusToSpawn.transform.position = _bonusSpot.transform.position;
            }
        }
        
        public void SubscribeOnTrackDisable(Action onTrackDisableHandler)
        {
            if (OnTrackDisable == null)
            {
                OnTrackDisable += onTrackDisableHandler;
            }
        }

        public void Enable()
        {
            gameObject.SetActive(true);
        }

        public void Disable()
        {
            OnTrackDisable?.Invoke();
            gameObject.SetActive(false);
        }

        public bool IsActive()
        {
            return gameObject.activeInHierarchy;
        }
    }
}